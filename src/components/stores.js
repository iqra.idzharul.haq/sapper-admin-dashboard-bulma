import { writable } from 'svelte/store';
export const isSidebarVisible = writable(true);
export const isContainer = writable(true);
export const isSignedIn = writable(false);
export const signInForm = writable({
  email: "",
  password: "",
});
