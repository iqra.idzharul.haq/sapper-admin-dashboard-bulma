export const signInUser = (user) =>{
  localStorage.setItem('user', user);
  localStorage.setItem('isSignedIn', true);
}

export const signOutUser = () =>{
  localStorage.clear();
}

export const getUser = () =>{
  return localStorage.getItem('user');
}

export const isSignedIn = () =>{
  return localStorage.getItem('isSignedIn');
}